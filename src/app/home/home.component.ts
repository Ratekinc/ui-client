import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { faExternalLinkAlt, faChartLine, faHeart, faCommentAlt, faGlobeAmericas, faClone, faVideo } from '@fortawesome/free-solid-svg-icons';
import { faFacebookF, faTwitter, faPinterestP, faTumblr, faVk, faRedditAlien, faTelegramPlane, faWhatsapp } from '@fortawesome/free-brands-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { NgxLinkifyOptions } from 'ngx-linkifyjs';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  faExternalLinkAlt = faExternalLinkAlt;
  faChartLine = faChartLine;
  faHeart = faHeart;
  faCommentAlt = faCommentAlt;
  faGlobeAmericas = faGlobeAmericas;
  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  faPinterestP = faPinterestP;
  faTumblr = faTumblr;
  faVk = faVk;
  faRedditAlien = faRedditAlien;
  faTelegramPlane = faTelegramPlane;
  faWhatsapp = faWhatsapp;
  faClone = faClone;
  faVideo = faVideo;
  total = 0;
  page = 1;
  users = [];
  posts = null;
  options = {
    addSuffix: true
  };
  facebookSharer = 'https://www.facebook.com/sharer.php?u=';
  twitterSharer = 'https://twitter.com/intent/tweet?text=';
  pinterestSharer = 'https://pinterest.com/pin/create/button/?url=';
  tumblrSharer = 'https://www.tumblr.com/widgets/share/tool?posttype=link&amp;canonicalUrl=';
  vkSharer = 'https://vk.com/share.php?url=';
  redditSharer = 'https://www.reddit.com/submit/?url=';
  telegramSharer = 'https://telegram.me/share/url?text=';
  whatsappSharer = 'whatsapp://send?text=';
  linkifyOptions:NgxLinkifyOptions = {
    attributes: 'routerLink',
    className: 'post-anchor',
    formatHref: function (href, type) {
      if (type === 'hashtag') {
        href = href.replace('#', '');
        return '/tag/'+href;
      }
      else if (type === 'mention') {
        href = href.replace('@', '');
        return '/account'+href;
      }
      else if (type === 'url') {
        return href;
      }
    },
  };
  loader = true;
  constructor(public http: HttpClient, public modalService: NgbModal, public meta: Meta, public title: Title) { }

  ngOnInit() {
    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/user').subscribe((data) => {
      this.loader = false;
      //document.getElementById('search').focus();
    });

    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/post?offset='+this.total).subscribe((response) => {
      this.posts = this.shuffle(response);
      console.log(this.posts);
      this.posts.forEach((post) => {
        if (typeof post.location === 'string')
        {
          post['location'] = JSON.parse(post.location);
        }
      });
      this.total = this.posts.length;
    });

    let titleDescription: string = 'Instagram viewer and analyzer - Gramlens.com';
    let metaDescription: string = 'Instagram viewer and analyzer for posts, followers, likes and statistics - Gramlens.com';
    this.meta.updateTag({ name: 'description', content: metaDescription });
    this.title.setTitle(titleDescription);
  }
  onScrollDown(ev) {

    // add another 20 items
    this.total++;

    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/post?offset='+this.total).subscribe((response) => {
      this.posts = this.posts.concat(this.shuffle(response));
      console.log(this.posts);
      this.posts.forEach((post) => {
        if (typeof post.location === 'string')
        {
          post['location'] = JSON.parse(post.location);
        }
      });
      this.total = this.posts.length;
    });
  }

  onScrollUp(ev) {
    this.total -= 20;
  }

  open(content, shortcode, captions) {
    this.modalService.open(content, {size: 'sm'}).result.then((result) => {
    }, (reason) => {
    });
    let postCaptionAll = '';
    captions.forEach((caption) => {
      postCaptionAll+=caption.node.text;
    });
    this.facebookSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode)+'&amp;p[title]='+encodeURIComponent(postCaptionAll);
    this.twitterSharer += encodeURIComponent(postCaptionAll);
    this.pinterestSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode)+'&amp;description='+encodeURIComponent(postCaptionAll);
    this.tumblrSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode)+'&amp;shareSource=tumblr_share_button';
    this.vkSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode);
    this.redditSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode);
    this.telegramSharer += encodeURIComponent(postCaptionAll)+'&amp;url='+('https://gramlens.com/single-post/'+shortcode);
    this.whatsappSharer += encodeURIComponent(postCaptionAll)+('https://gramlens.com/single-post/'+shortcode);
  }

  shuffle(data) {
      return data.sort(() => Math.random() - 0.5);
  }
}
