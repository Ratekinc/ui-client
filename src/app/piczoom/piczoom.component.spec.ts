import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PiczoomComponent } from './piczoom.component';

describe('PiczoomComponent', () => {
  let component: PiczoomComponent;
  let fixture: ComponentFixture<PiczoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PiczoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PiczoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
