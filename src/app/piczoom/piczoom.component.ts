import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {response} from "express";
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-piczoom',
  templateUrl: './piczoom.component.html',
  styleUrls: ['./piczoom.component.css']
})
export class PiczoomComponent implements OnInit {
  img = null;
  users = null;
  loader = false;
  notFound = false;
  loader1 = true;
  constructor(public http: HttpClient, public meta: Meta, public title: Title) { }

  ngOnInit(): void {
    setTimeout(() => {
      this.loader1 = false;
    }, 300);

    let titleDescription: string = 'Instagram profile photo viewer - Gramlens.com';
    let metaDescription: string = 'Instagram dp full size, profile photo zoom, private photo viewer - Gramlens.com';
    this.meta.updateTag({ name: 'description', content: metaDescription });
    this.meta.addTag({ name: 'keywords', content: 'instadp, insta dp, igdp, profile picture viewer, instagram display picture, full size, instagram, display, picture, view, profile, picture, full HD profile photo viewer, instagram profile picture viewer' });
    this.title.setTitle(titleDescription);
  }

  searchUser(value) {
    this.loader = true;
    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/blocked-user/' + value).subscribe((data) => {
      let response: any;
      response = data;
      if (response.length == 0)
      {
        this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/user/profile-pic-hd/' + value).subscribe((response) => {
          let img = response;
          this.loader = false;
          this.img = img['data'];
        }, (error) => {
          this.loader = false;
          this.notFound = true
        });
      }
      else
      {
        this.loader = false;
        this.notFound = true
      }
    }, (error) => {
      this.loader = false;
      this.notFound = true;
    });

  }

  userList(event) {
    this.img = null;
    this.users = null;
    this.loader = true;
    let new_value = event.target.value.replace(/[&\/\\#,+()$~%'":*?<>{}!^=]/g, '');
    const url = 'https://www.instagram.com/web/search/topsearch/?context=user';
    this.http.get(url + '&query=' + new_value + '&include_reel=true').subscribe(response => {
      this.users = response;
      this.users = this.users.users;
      this.loader = false;
      this.notFound = false;
      if (this.users.length === 0)
      {
        this.notFound = true;
      }
    }, (error) => {
      this.notFound = true;
    });
  }
}
