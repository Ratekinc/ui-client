import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser'

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  tab = 'about';
  username = '';
  aboutContent = null;
  cantSubmit = false;
  done = false;
  loader = true;
  constructor(public http: HttpClient, public domSanitizer: DomSanitizer) { }

  ngOnInit() {
    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/about-content').subscribe((response) => {
      this.aboutContent = response;
      this.aboutContent.forEach((item) => {
        item.content = this.domSanitizer.bypassSecurityTrustHtml(item.content);
      });
      this.loader = false;
    });
  }
  submit(value) {
    this.done = false;
    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/blocked-user/24-hour-check').subscribe((response) => {
      let data;
      data = response;
      if (data.length)
      {
        this.done = true;
        this.cantSubmit = true;
      }
      else
      {
        this.http.post('https://phpstack-469914-1474997.cloudwaysapps.com/api/blocked-user/store', value).subscribe((response) => {
          this.username = '';
          this.done = true;
        });
      }
    });
  }
}
