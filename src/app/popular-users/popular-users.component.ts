import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { timeout } from 'rxjs/operators';

@Component({
  selector: 'app-popular-users',
  templateUrl: './popular-users.component.html',
  styleUrls: ['./popular-users.component.css']
})
export class PopularUsersComponent implements OnInit {
  users = null;
  constructor(public http: HttpClient) { }

  ngOnInit() {
    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/top-user').subscribe((response) => {
      this.users = response;
      //this.users.sort(() => Math.random() - 0.5);
      this.users.length = 12;
      this.users.forEach((user) => {
        // user['profile_pic'] = 'https://phpstack-469914-1474997.cloudwaysapps.com/assets/images/' + user['profile_pic'];
        // this.http.get('https://www.instagram.com/' + user.username + '?__a=1').subscribe((data) => {
        //   let userData:any;
        //   userData = data;
        //   user['profile_pic'] = userData.graphql.user.profile_pic_url;
        // });
      });
    });
  }

}
