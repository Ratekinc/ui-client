import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { faCommentAlt, faExternalLinkAlt, faHeart } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, Router } from '@angular/router';
import { faFacebookF, faTwitter, faPinterestP, faTumblr, faVk, faRedditAlien, faTelegramPlane, faWhatsapp } from '@fortawesome/free-brands-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxLinkifyOptions } from 'ngx-linkifyjs';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {
  faExternalLinkAlt = faExternalLinkAlt;
  faHeart = faHeart;
  faCommentAlt = faCommentAlt;
  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  faPinterestP = faPinterestP;
  faTumblr = faTumblr;
  faVk = faVk;
  faRedditAlien = faRedditAlien;
  faTelegramPlane = faTelegramPlane;
  faWhatsapp = faWhatsapp;
  total = 20;
  tag = this.activatedRoute.snapshot.paramMap.get('tag');
  tagData = null;
  variables = null;
  event = {};
  facebookSharer = 'https://www.facebook.com/sharer.php?u=';
  twitterSharer = 'https://twitter.com/intent/tweet?text=';
  pinterestSharer = 'https://pinterest.com/pin/create/button/?url=';
  tumblrSharer = 'https://www.tumblr.com/widgets/share/tool?posttype=link&amp;canonicalUrl=';
  vkSharer = 'https://vk.com/share.php?url=';
  redditSharer = 'https://www.reddit.com/submit/?url=';
  telegramSharer = 'https://telegram.me/share/url?text=';
  whatsappSharer = 'whatsapp://send?text=';
  linkifyOptions:NgxLinkifyOptions = {
    attributes: 'routerLink',
    className: 'post-anchor',
    formatHref: function (href, type) {
      if (type === 'hashtag') {
        href = href.replace('#', '');
        return '/tag/'+href;
      }
      else if (type === 'mention') {
        href = href.replace('@', '');
        return '/account'+href;
      }
      else if (type === 'url') {
        return href;
      }
    },
  };
  loader = true;
  constructor(public http: HttpClient, public activatedRoute: ActivatedRoute, public router: Router, public modalService: NgbModal, public meta: Meta, public title: Title) {
    this.activatedRoute.params.subscribe((params) => {
      if (params.tag) {
        this.tag = params.tag;
      }
    });
  }

  ngOnInit() {
    this.http.get('https://www.instagram.com/explore/tags/' + this.tag + '?__a=1').subscribe((data) => {
      this.tagData = data;
      this.tagData = this.tagData.graphql.hashtag;
      this.variables = {tag_name: this.tag, first: 12, after: this.tagData.edge_hashtag_to_media.page_info.end_cursor};
      this.loader = false;

      let description: string = "Explore #"+this.tag+" Instagram posts, accounts, trending posts, shoutouts, trending photos, followers, likes and locations | Gramlens.com";
      this.meta.updateTag({ name: 'description', content: description });
      this.title.setTitle(description);
      this.meta.addTag({property:'og:image', content:this.tagData.profile_pic_url });
      this.meta.addTag({property:'og:keywords', content:this.tag+", "+this.tag+" photos"+", "+this.tag+" posts"+", "+this.tag+" trending posts"+", "+this.tag+" trending photos" });
      this.meta.addTag({property:'og:description', content:"Explore #"+this.tag+" Instagram posts, accounts, trending posts, shoutouts, trending photos, followers, likes and locations | Gramlens.com" });
    });
  }
  onScrollDown(ev) {
    if (this.tagData.edge_hashtag_to_top_posts.edges.length < this.tagData.edge_hashtag_to_top_posts.count)
    {
      const hash = '7dabc71d3e758b1ec19ffb85639e427b';
      const url = 'https://www.instagram.com/graphql/query/?query_hash=';
      let newData = null;
      // add another 20 items
      this.total += 20;
      this.http.get(url + hash + '&variables=' + encodeURIComponent(JSON.stringify(this.variables))).subscribe((data) => {
        newData = data;
        const edges = newData.data.hashtag.edge_hashtag_to_top_posts.edges;
        this.tagData.edge_hashtag_to_top_posts.edges = this.tagData.edge_hashtag_to_top_posts.edges.concat(edges);
        this.variables.after = newData.data.hashtag.edge_hashtag_to_top_posts.page_info.end_cursor;
      });
    }
  }

  onScrollUp(ev) {
    this.total -= 20;
  }

  open(content, shortcode, captions) {
    this.modalService.open(content, {size: 'sm'}).result.then((result) => {
    }, (reason) => {
    });
    let postCaptionAll = '';
    captions.forEach((caption) => {
      postCaptionAll+=caption.node.text;
    });
    this.facebookSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode)+'&amp;p[title]='+encodeURIComponent(postCaptionAll);
    this.twitterSharer += encodeURIComponent(postCaptionAll);
    this.pinterestSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode)+'&amp;description='+encodeURIComponent(postCaptionAll);
    this.tumblrSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode)+'&amp;shareSource=tumblr_share_button';
    this.vkSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode);
    this.redditSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode);
    this.telegramSharer += encodeURIComponent(postCaptionAll)+'&amp;url='+('https://gramlens.com/single-post/'+shortcode);
    this.whatsappSharer += encodeURIComponent(postCaptionAll)+('https://gramlens.com/single-post/'+shortcode);
  }

}
