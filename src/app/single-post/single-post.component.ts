import { Component, OnInit } from '@angular/core';
import { faExternalLinkAlt, faChartLine, faHeart, faCommentAlt, faGlobeAmericas, faCloudDownloadAlt } from '@fortawesome/free-solid-svg-icons';
import { faFacebookF, faTwitter, faPinterestP, faTumblr, faVk, faRedditAlien, faTelegramPlane, faWhatsapp } from '@fortawesome/free-brands-svg-icons';
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxLinkifyOptions } from 'ngx-linkifyjs';
import { Meta, Title } from '@angular/platform-browser';
import * as bigInt from 'big-integer';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.css']
})
export class SinglePostComponent implements OnInit {
  faExternalLinkAlt = faExternalLinkAlt;
  faChartLine = faChartLine;
  faHeart = faHeart;
  faCommentAlt = faCommentAlt;
  faGlobeAmericas = faGlobeAmericas;
  faCloudDownloadAlt = faCloudDownloadAlt;
  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  faPinterestP = faPinterestP;
  faTumblr = faTumblr;
  faVk = faVk;
  faRedditAlien = faRedditAlien;
  faTelegramPlane = faTelegramPlane;
  faWhatsapp = faWhatsapp;
  total = 20;
  id = this.activatedRoute.snapshot.paramMap.get('id');
  postData = null;
  options = {
    addSuffix: true
  };
  imageIndex = 0;
  trendingTags = null;
  facebookSharer = 'https://www.facebook.com/sharer.php?u=';
  twitterSharer = 'https://twitter.com/intent/tweet?text=';
  pinterestSharer = 'https://pinterest.com/pin/create/button/?url=';
  tumblrSharer = 'https://www.tumblr.com/widgets/share/tool?posttype=link&amp;canonicalUrl=';
  vkSharer = 'https://vk.com/share.php?url=';
  redditSharer = 'https://www.reddit.com/submit/?url=';
  telegramSharer = 'https://telegram.me/share/url?text=';
  whatsappSharer = 'whatsapp://send?text=';
  linkifyOptions:NgxLinkifyOptions = {
    attributes: 'routerLink',
    className: 'post-anchor',
    formatHref: function (href, type) {
      if (type === 'hashtag') {
        href = href.replace('#', '');
        return '/tag/'+href;
      }
      else if (type === 'mention') {
        href = href.replace('@', '');
        return '/account'+href;
      }
      else if (type === 'url') {
        return href;
      }
    },
  };
  constructor(public activatedRoute: ActivatedRoute, public router: Router, public http: HttpClient, public modalService: NgbModal, public meta: Meta, public title: Title) {
    this.id = this.idToShortcode(this.id);
  }

  ngOnInit() {
    const hash = 'eaffee8f3c9c089c9904a5915a898814';
    const url = 'https://www.instagram.com/graphql/query/?query_hash=';
    const variables = {shortcode: this.id, child_comment_count: 3, fetch_comment_count: 40, parent_comment_count: 40, has_threaded_comments: true}
    this.http.get(url + hash + '&variables=' + encodeURIComponent(JSON.stringify(variables))).subscribe((data) => {
      this.postData = data;
      this.postData = this.postData.data.shortcode_media;
      let postCaptionAll = '';
      this.postData.edge_media_to_caption.edges.forEach((caption) => {
        postCaptionAll += caption.node.text;
      });
      this.facebookSharer += encodeURIComponent('https://gramlens.com/single-post/CDjMCNEF0yu')+'&amp;p[title]='+encodeURIComponent(postCaptionAll);
      this.twitterSharer += encodeURIComponent(postCaptionAll);
      this.pinterestSharer += encodeURIComponent('https://gramlens.com/single-post/CDjMCNEF0yu')+'&amp;description='+encodeURIComponent(postCaptionAll);
      this.tumblrSharer += encodeURIComponent('https://gramlens.com/single-post/CDjMCNEF0yu')+'&amp;shareSource=tumblr_share_button';
      this.vkSharer += encodeURIComponent('https://gramlens.com/single-post/CDjMCNEF0yu');
      this.redditSharer += encodeURIComponent('https://gramlens.com/single-post/CDjMCNEF0yu');
      this.telegramSharer += encodeURIComponent(postCaptionAll)+'&amp;url='+('https://gramlens.com/single-post/CDjMCNEF0yu');
      this.whatsappSharer += encodeURIComponent(postCaptionAll)+('https://gramlens.com/single-post/CDjMCNEF0yu');
      let description: string = 'Instagram @'+this.postData.owner.username+' '+postCaptionAll+'- Gramlens.com';
      this.meta.updateTag({ name: 'description', content: description });
      if(this.postData.__typename == 'GraphImage'){
        this.meta.addTag({property:'og:image', content:this.postData.display_url});
      }
    });
    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/tag').subscribe((response) => {
      this.trendingTags = response;
    });
  }

  onScrollDown(ev) {

    // add another 20 items
    this.total += 20;
  }

  onScrollUp(ev) {
    this.total -= 20;
  }

  onSlide(event) {
    let current = event.current.split('-');
    this.imageIndex = parseInt(current[2])
  }

  open(content) {
    this.modalService.open(content, {size: 'sm'}).result.then((result) => {
    }, (reason) => {
    });
  }

  idToShortcode(instagram_id) {
    let alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_';
    let bigint_alphabet = '0123456789abcdefghijklmnopqrstuvwxyz';
    let o = bigInt( instagram_id ).toString( 64 )

    return o.replace(/<(\d+)>|(\w)/g, (m,m1,m2) =>
    {
      return alphabet.charAt((m1) ? parseInt(m1) : bigint_alphabet.indexOf(m2))
    });

  }

}
