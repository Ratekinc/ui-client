import { Component, OnInit } from '@angular/core';
import { faList } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-collection-list',
  templateUrl: './collection-list.component.html',
  styleUrls: ['./collection-list.component.css']
})
export class CollectionListComponent implements OnInit {
  faList = faList;
  collections = null;
  constructor(public http: HttpClient, public router: Router) { }

  ngOnInit() {
    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/editor-list').subscribe((response) => {
      this.collections = response;
    });
  }

}
