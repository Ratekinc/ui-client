import { Component, OnInit } from '@angular/core';
import { faExternalLinkAlt, faHeart, faCommentAlt, faGlobeAmericas } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  faExternalLinkAlt = faExternalLinkAlt;
  faHeart = faHeart;
  faCommentAlt = faCommentAlt;
  faGlobeAmericas = faGlobeAmericas;
  constructor() { }

  ngOnInit() {
  }

}
