import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { faCommentAlt, faExternalLinkAlt, faHeart } from '@fortawesome/free-solid-svg-icons';
import { DomSanitizer } from "@angular/platform-browser";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxLinkifyOptions } from 'ngx-linkifyjs';
import { faFacebookF, faPinterestP, faRedditAlien, faTelegramPlane, faTumblr, faTwitter, faVk, faWhatsapp } from "@fortawesome/free-brands-svg-icons";

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {
  faExternalLinkAlt = faExternalLinkAlt;
  faHeart = faHeart;
  faCommentAlt = faCommentAlt;
  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  faPinterestP = faPinterestP;
  faTumblr = faTumblr;
  faVk = faVk;
  faRedditAlien = faRedditAlien;
  faTelegramPlane = faTelegramPlane;
  faWhatsapp = faWhatsapp;
  locData = null;
  location = this.activatedRoute.snapshot.paramMap.get('loc');
  id = this.activatedRoute.snapshot.paramMap.get('id');
  variables = null;
  facebookSharer = 'https://www.facebook.com/sharer.php?u=';
  twitterSharer = 'https://twitter.com/intent/tweet?text=';
  pinterestSharer = 'https://pinterest.com/pin/create/button/?url=';
  tumblrSharer = 'https://www.tumblr.com/widgets/share/tool?posttype=link&amp;canonicalUrl=';
  vkSharer = 'https://vk.com/share.php?url=';
  redditSharer = 'https://www.reddit.com/submit/?url=';
  telegramSharer = 'https://telegram.me/share/url?text=';
  whatsappSharer = 'whatsapp://send?text=';
  linkifyOptions:NgxLinkifyOptions = {
    attributes: 'routerLink',
    className: 'post-anchor',
    formatHref: function (href, type) {
      if (type === 'hashtag') {
        href = href.replace('#', '');
        return '/tag/'+href;
      }
      else if (type === 'mention') {
        href = href.replace('@', '');
        return '/account'+href;
      }
      else if (type === 'url') {
        return href;
      }
    },
  };

  constructor(public http: HttpClient, public activatedRoute: ActivatedRoute, public router: Router, public sanitizer: DomSanitizer, public modalService: NgbModal) {
      this.activatedRoute.params.subscribe((params) => {
          if (params.loc) {
              this.id = params.id;
              this.location = params.loc;
          }
      });
  }

  ngOnInit() {
      this.http.get('https://www.instagram.com/explore/locations/' + this.id + '/' + this.location + '?__a=1').subscribe((data) => {
          this.locData = data;
          this.locData = this.locData.graphql.location;
          this.variables = {id: this.id, first: 12, after: this.locData.edge_location_to_media.page_info.end_cursor};
      });
  }

  onScrollDown(ev) {
      if (this.locData.edge_location_to_media.edges.length < this.locData.edge_location_to_media.count)
      {
          const hash = '36bd0f2bf5911908de389b8ceaa3be6d';
          const url = 'https://www.instagram.com/graphql/query/?query_hash=';
          let newData = null;
          // add another 20 items
          this.http.get(url + hash + '&variables=' + encodeURIComponent(JSON.stringify(this.variables))).subscribe((data) => {
              newData = data;
              const edges = newData.data.location.edge_location_to_media.edges;
              this.locData.edge_location_to_media.edges = this.locData.edge_location_to_media.edges.concat(edges);
              this.variables.after = newData.data.location.edge_location_to_media.page_info.end_cursor;
          });
      }
  }

  cleanURL(oldURL: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(oldURL);
  }

  open(content, shortcode, captions) {
    this.modalService.open(content, {size: 'sm'}).result.then((result) => {
    }, (reason) => {
    });
    let postCaptionAll = '';
    captions.forEach((caption) => {
      postCaptionAll+=caption.node.text;
    });
    this.facebookSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode)+'&amp;p[title]='+encodeURIComponent(postCaptionAll);
    this.twitterSharer += encodeURIComponent(postCaptionAll);
    this.pinterestSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode)+'&amp;description='+encodeURIComponent(postCaptionAll);
    this.tumblrSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode)+'&amp;shareSource=tumblr_share_button';
    this.vkSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode);
    this.redditSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode);
    this.telegramSharer += encodeURIComponent(postCaptionAll)+'&amp;url='+('https://gramlens.com/single-post/'+shortcode);
    this.whatsappSharer += encodeURIComponent(postCaptionAll)+('https://gramlens.com/single-post/'+shortcode);
  }
}
