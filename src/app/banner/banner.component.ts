import { Component, OnInit } from '@angular/core';
import { faSearch, faHashtag, faMapMarkedAlt } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  faSearch = faSearch;
  faHashtag = faHashtag;
  faMapMarkedAlt = faMapMarkedAlt;
  query = '';
  variables = {
    context: 'blended',
    rank_token: 0.43922183880641263,
    query: this.query,
    include_reel: true
  };
  data: {};
  constructor(public route: Router, public http: HttpClient) { }

  ngOnInit() {
    document.getElementById('search').focus();
  }

  searchInstagram(event) {
    this.route.navigate(['/search', 'user', event.target.value]);
  }

}
