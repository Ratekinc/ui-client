import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-editor-list',
  templateUrl: './editor-list.component.html',
  styleUrls: ['./editor-list.component.css']
})
export class EditorListComponent implements OnInit {
  list = null;
  color = [
    '#ffa500',
    '#00a5dc',
    '#004eaf',
    '#2db928',
    '#057855',
    '#ff2d37',
    '#ffdd00',
    '#c1d82f',
    '#e4002b',
    '#a51890',
    '#0ebeff',
    '#d900d9'
  ];
  constructor(public http: HttpClient) { }

  ngOnInit() {
    this.select();
  }

  select() {
    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/editor-list').subscribe((response) => {
      this.list = response;
      this.list.sort(() => Math.random() - 0.5)
    });
  }
}
