import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.css']
})
export class CollectionComponent implements OnInit {
  users = null;
  collection = null;
  collections = null;
  id = this.activatedRoute.snapshot.paramMap.get('id');
  constructor(public http: HttpClient, public activatedRoute: ActivatedRoute, public router: Router) { }

  ngOnInit() {
    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/editor-list/get/' + this.id).subscribe((response) => {
      this.collection = response[0];
    });
    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/editor-list').subscribe((response) => {
      this.collections = response;
      this.collections.sort(() => Math.random() - 0.5)
    });
    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/top-user/get-by-collection/' + this.id).subscribe((response) => {
      this.users = response;
    });
  }

}
