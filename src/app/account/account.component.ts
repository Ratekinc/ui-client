import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { faExternalLinkAlt, faChartLine, faHeart, faCommentAlt, faGlobeAmericas, faClone, faVideo } from '@fortawesome/free-solid-svg-icons';
import { faFacebookF, faTwitter, faPinterestP, faTumblr, faVk, faRedditAlien, faTelegramPlane, faWhatsapp } from '@fortawesome/free-brands-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxLinkifyOptions } from 'ngx-linkifyjs';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  faExternalLinkAlt = faExternalLinkAlt;
  faChartLine = faChartLine;
  faHeart = faHeart;
  faCommentAlt = faCommentAlt;
  faGlobeAmericas = faGlobeAmericas;
  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  faPinterestP = faPinterestP;
  faTumblr = faTumblr;
  faVk = faVk;
  faRedditAlien = faRedditAlien;
  faTelegramPlane = faTelegramPlane;
  faWhatsapp = faWhatsapp;
  faClone = faClone;
  faVideo = faVideo;
  total = 20;
  username = this.activatedRoute.snapshot.paramMap.get('username');
  userData = null;
  variables = null;
  options = {
    addSuffix: true
  };
  facebookSharer = 'https://www.facebook.com/sharer.php?u=';
  twitterSharer = 'https://twitter.com/intent/tweet?text=';
  pinterestSharer = 'https://pinterest.com/pin/create/button/?url=';
  tumblrSharer = 'https://www.tumblr.com/widgets/share/tool?posttype=link&amp;canonicalUrl=';
  vkSharer = 'https://vk.com/share.php?url=';
  redditSharer = 'https://www.reddit.com/submit/?url=';
  telegramSharer = 'https://telegram.me/share/url?text=';
  whatsappSharer = 'whatsapp://send?text=';
  averageLikes = 0;
  averageComments = 0;
  engagementRate = 0;
  linkifyOptions:NgxLinkifyOptions = {
    attributes: 'routerLink',
    className: 'post-anchor',
    formatHref: function (href, type) {
      if (type === 'hashtag') {
        href = href.replace('#', '');
        return '/tag/'+href;
      }
      else if (type === 'mention') {
        href = href.replace('@', '');
        return '/account'+href;
      }
      else if (type === 'url') {
        return href;
      }
    },
  };
  notFound = false;
  loader = true;
  posts = [];
  constructor(public http: HttpClient, public activatedRoute: ActivatedRoute, public modalService: NgbModal, public meta: Meta, public title: Title) { }

  ngOnInit() {
    this.notFound = false;
    this.userData = {is_private: null};
    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/blocked-user/' + this.username).subscribe((data) => {
      let response: any;
      response = data;
      if (response.length == 0)
      {
        this.http.get('https://www.instagram.com/' + this.username + '?__a=1').subscribe((data) => {
          this.userData = data;
          this.userData = this.userData.graphql.user;
          this.variables = {id: this.userData.id, first: 12, after: this.userData.edge_owner_to_timeline_media.page_info.end_cursor};
          this.averageLikes = Math.round(this.userData.edge_owner_to_timeline_media.edges.reduce((a, b) => a + b.node.edge_media_preview_like.count, 0) / 12);
          this.averageComments = Math.round(this.userData.edge_owner_to_timeline_media.edges.reduce((a, b) => a + b.node.edge_media_to_comment.count, 0) / 12);
          this.engagementRate = Math.round(((this.userData.edge_owner_to_timeline_media.edges.reduce((a, b) => a + b.node.edge_media_preview_like.count, 0) + this.userData.edge_owner_to_timeline_media.edges.reduce((a, b) => a + b.node.edge_media_to_comment.count, 0)) / this.userData.edge_followed_by.count) * 100 );
          this.loader = false;
          let description: string = 'Instagram @'+this.userData.username+' posts, followers, likes and statistics - Gramlens.com';
          this.meta.updateTag({ name: 'description', content: description });
          this.title.setTitle(description);
          this.meta.addTag({property:'og:image', content:this.userData.profile_pic_url_hd });
        }, (error) => {
          this.notFound = true;
          this.loader = false;
          this.loadTopPosts();
        });
      }
      else
      {
        this.notFound = true;
        this.loader = false;
        this.loadTopPosts();
      }
    }, (error) => {
      this.notFound = true;
      this.loader = false;
      this.loadTopPosts();
    });

  }

  onScrollDown(ev) {
    if (this.userData.edge_owner_to_timeline_media.edges.length < this.userData.edge_owner_to_timeline_media.count)
    {
      const hash = 'e769aa130647d2354c40ea6a439bfc08';
      const url = 'https://www.instagram.com/graphql/query/?query_hash=';
      let newData = null;
      // add another 20 items
      this.total += 20;
      this.http.get(url + hash + '&variables=' + encodeURIComponent(JSON.stringify(this.variables))).subscribe((data) => {
        newData = data;
        const edges = newData.data.user.edge_owner_to_timeline_media.edges;
        this.userData.edge_owner_to_timeline_media.edges = this.userData.edge_owner_to_timeline_media.edges.concat(edges);
        this.variables.after = newData.data.user.edge_owner_to_timeline_media.page_info.end_cursor;
      });
      let description: string = 'Instagram @'+this.userData.username+' posts, followers, likes and statistics - Gramlens.com';
      this.meta.updateTag({ name: 'description', content: description });
    }
  }

  loadTopPosts(){
    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/post?offset='+this.total).subscribe((response) => {
      this.posts = this.shuffle(response);
      console.log(this.posts);
      this.posts.forEach((post) => {
        if (typeof post.location === 'string')
        {
          post['location'] = JSON.parse(post.location);
        }
      });
      this.total = this.posts.length;
    });
  }

  shuffle(data) {
    return data.sort(() => Math.random() - 0.5);
  }

  onScrollForMorePosts(ev){
    this.total++;

    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/post?offset='+this.total).subscribe((response) => {
      this.posts = this.posts.concat(this.shuffle(response));
      console.log(this.posts);
      this.posts.forEach((post) => {
        if (typeof post.location === 'string')
        {
          post['location'] = JSON.parse(post.location);
        }
      });
      this.total = this.posts.length;
    });
  }

  onScrollUp(ev) {
    this.total -= 20;
  }

  open(content, shortcode, captions) {
    this.modalService.open(content, {size: 'sm'}).result.then((result) => {
    }, (reason) => {
    });
    let postCaptionAll = '';
    captions.forEach((caption) => {
      postCaptionAll+=caption.node.text;
    });
    this.facebookSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode)+'&amp;p[title]='+encodeURIComponent(postCaptionAll);
    this.twitterSharer += encodeURIComponent(postCaptionAll);
    this.pinterestSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode)+'&amp;description='+encodeURIComponent(postCaptionAll);
    this.tumblrSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode)+'&amp;shareSource=tumblr_share_button';
    this.vkSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode);
    this.redditSharer += encodeURIComponent('https://gramlens.com/single-post/'+shortcode);
    this.telegramSharer += encodeURIComponent(postCaptionAll)+'&amp;url='+('https://gramlens.com/single-post/'+shortcode);
    this.whatsappSharer += encodeURIComponent(postCaptionAll)+('https://gramlens.com/single-post/'+shortcode);
  }
}
