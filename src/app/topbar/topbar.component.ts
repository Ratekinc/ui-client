import { Component, OnInit } from '@angular/core';
import { faHome, faSearch, faUsers, faFileImage, faTags, faSearchPlus } from '@fortawesome/free-solid-svg-icons';
import {HttpClient} from "@angular/common/http";
import "@fortawesome/fontawesome-svg-core/styles.css";

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

  isNavbarCollapsed = true;
  faHome = faHome;
  faSearchPlus = faSearchPlus;
  faSearch = faSearch;
  faUsers = faUsers;
  faFileImage = faFileImage;
  faTags = faTags;
  users = null;
  trendingTags = null;
  list = null;
  constructor(public http: HttpClient) { }

  ngOnInit() {
    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/top-user').subscribe((response) => {
      this.users = response;
    });
    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/tag').subscribe((response) => {
      this.trendingTags = response;
    });
    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/editor-list').subscribe((response) => {
      this.list = response;
    });
  }

  focusInputSearch() {
    document.getElementById('search').focus();
  }
}
