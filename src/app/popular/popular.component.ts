import { Component, OnInit } from '@angular/core';
import { faLocationArrow } from '@fortawesome/free-solid-svg-icons';
import {Router, Event, NavigationEnd} from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-popular',
  templateUrl: './popular.component.html',
  styleUrls: ['./popular.component.css']
})
export class PopularComponent implements OnInit {

  tab = 'user';
  total = 20;
  faLocationArrow = faLocationArrow;
  trendingTags = null;
  trendingLocations = null;
  users = null;
  tagData = null;
  locationData = null;
  loader = true;
  constructor(public router: Router, public http: HttpClient) {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        const url = this.router.url.split('/');
        if (url[url.length - 1] !== 'popular') {
          this.tab = url[url.length - 1];
        }
      }
    });
  }

  ngOnInit() {
    const url = this.router.url.split('/');
    if (url[url.length - 1] !== 'popular') {
      this.tab = url[url.length - 1];
    }
    this.getData(this.tab);
  }

  changeTab(value) {
    this.tab = value;
    this.getData(value)
  }

  getData(tab) {
    if (tab == 'user') {
      this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/top-user').subscribe((response) => {
        this.users = response;
        this.users.forEach((user) => {
          // user['profile_pic'] = 'https://phpstack-469914-1474997.cloudwaysapps.com/assets/images/' + user['profile_pic'];
          // this.http.get('https://www.instagram.com/' + user.username + '?__a=1').subscribe((data) => {
          //   let userData:any;
          //   userData = data;
          //   user['profile_pic'] = userData.graphql.user.profile_pic_url;
          //   user['followers'] = userData.graphql.user.edge_followed_by.count;
          this.loader = false;
          //});
        });
      });
    } else if (tab == 'tag') {
      this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/tag').subscribe((response) => {
        this.trendingTags = response;
        this.tagData = [];
        this.trendingTags.forEach((tag, index) => {
          // this.http.get('https://www.instagram.com/explore/tags/' + tag.name + '?__a=1').subscribe((response) => {
          //   let data = response['graphql'];
          //   this.tagData.push({name: data.hashtag.name, posts: data.hashtag.edge_hashtag_to_media.count});
          //   this.loader = false;
          // });
          this.tagData.push({name:tag.name, posts: tag.posts});
          this.loader = false;
        });
      });
    } else {
      this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/top-location').subscribe((response) => {
        this.trendingLocations = response;
        this.locationData = [];
        this.trendingLocations.forEach((location, index) => {
          // this.http.get('https://www.instagram.com/explore/locations/' + location.slug + '?__a=1').subscribe((response) => {
          //   let data = response['graphql'];
          //   this.locationData.push({id: data.location.id, name: data.location.name, slug: data.location.slug, posts: data.location.edge_location_to_media.count});
          //   this.loader = false;
          // });
          this.locationData.push({id: location.id, name: location.name, slug: location.slug, posts: location.posts});
          this.loader = false;
        });
      });
    }
  }

  onScrollDown(ev) {
    // add another 20 items
    this.total += 20;
  }

  onScrollUp(ev) {
    this.total -= 20;
  }

}
