import { Component, OnInit } from '@angular/core';
import { faLocationArrow } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-nearby-search',
  templateUrl: './nearby-search.component.html',
  styleUrls: ['./nearby-search.component.css']
})
export class NearbySearchComponent implements OnInit {
  total = 20;
  blockedUsers: any;
  faLocationArrow = faLocationArrow;
  input = '';
  enterPressed = false;
  tab = 'place';
  userData = null;
  tagData = null;
  locationData = null;
  notFound = false;
  loader = true;
  constructor(public activatedRoute: ActivatedRoute, public http: HttpClient, public router: Router) {
    this.activatedRoute.params.subscribe((params) => {
      if (params.tab) {
        this.enterPressed = true;
        this.tab = params.tab;
        this.input = params.value;
        let new_value = params.value.replace(/[&\/\\#,+()$~%'":*?<>{}!^=]/g, '');
        const url = 'https://www.instagram.com/web/search/topsearch/?context=' + this.tab;
        this.http.get(url + '&query=' + new_value + '&include_reel=true').subscribe((data) => {
          this.notFound = false;
          this.loader = false;
          if (params.tab === 'user') {
            this.userData = data;
            this.userData = this.userData.users;
            if (this.userData.length === 0)
            {
              this.notFound = true
            }
          } else if (params.tab === 'hashtag') {
            this.tagData = data;
            this.tagData = this.tagData.hashtags;
            if (this.tagData.length === 0)
            {
              this.notFound = true
            }
          } else {
            this.locationData = data;
            this.locationData = this.locationData.places;
            if (this.locationData.length === 0)
            {
              this.notFound = true
            }
          }
        }, (error) => {
          this.notFound = true;
        });
      }
    });
  }

  ngOnInit() {
  }

  testFn() {
    this.enterPressed = true;
    this.router.navigate(['/search', this.tab, this.input]);
  }

  changeTab(event, value) {
    event.preventDefault();
    this.router.navigate(['/search', value, this.input]);
  }

  onScrollDown(ev) {

    // add another 20 items
    this.total += 20;
  }

  onScrollUp(ev) {
    this.total -= 20;
  }

}
