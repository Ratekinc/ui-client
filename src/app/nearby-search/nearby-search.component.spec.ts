import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NearbySearchComponent } from './nearby-search.component';

describe('NearbySearchComponent', () => {
  let component: NearbySearchComponent;
  let fixture: ComponentFixture<NearbySearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NearbySearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NearbySearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
