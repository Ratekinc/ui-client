import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-trend-hashtag',
  templateUrl: './trend-hashtag.component.html',
  styleUrls: ['./trend-hashtag.component.css']
})
export class TrendHashtagComponent implements OnInit {
  trendingTags = null;
  constructor(public http: HttpClient) { }

  ngOnInit() {
    this.select();
  }

  select() {
    this.http.get('https://phpstack-469914-1474997.cloudwaysapps.com/api/tag').subscribe((response) => {
      this.trendingTags = response;
    });
  }
}
