import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendHashtagComponent } from './trend-hashtag.component';

describe('TrendHashtagComponent', () => {
  let component: TrendHashtagComponent;
  let fixture: ComponentFixture<TrendHashtagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrendHashtagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendHashtagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
