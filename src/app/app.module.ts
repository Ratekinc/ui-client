import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatInputModule } from '@angular/material/input';
import { NbThemeModule } from '@nebular/theme';
import { NgxLinkifyjsModule } from 'ngx-linkifyjs';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RouterModule, Routes } from '@angular/router';
import { TopbarComponent } from './topbar/topbar.component';
import { BannerComponent } from './banner/banner.component';
import { EditorListComponent } from './editor-list/editor-list.component';
import { TrendHashtagComponent } from './trend-hashtag/trend-hashtag.component';
import { PopularUsersComponent } from './popular-users/popular-users.component';
import { PostComponent } from './post/post.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoaderComponent } from './loader/loader.component';
import { CollectionComponent } from './collection/collection.component';
import { HomeComponent } from './home/home.component';
import { TagComponent } from './tag/tag.component';
import { AccountComponent } from './account/account.component';
import { SinglePostComponent } from './single-post/single-post.component';
import { PopularComponent } from './popular/popular.component';
import { CollectionListComponent } from './collection-list/collection-list.component';
import { AboutComponent } from './about/about.component';
import { NearbySearchComponent } from './nearby-search/nearby-search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { LocationComponent } from './location/location.component';
import { SearchComponent } from './search/search.component';
import { DateFnsModule } from 'ngx-date-fns';
import { PiczoomComponent } from './piczoom/piczoom.component';

@NgModule({
  declarations: [
    AppComponent,
    TopbarComponent,
    BannerComponent,
    EditorListComponent,
    TrendHashtagComponent,
    PopularUsersComponent,
    PostComponent,
    LoaderComponent,
    CollectionComponent,
    HomeComponent,
    TagComponent,
    AccountComponent,
    SinglePostComponent,
    PopularComponent,
    CollectionListComponent,
    AboutComponent,
    NearbySearchComponent,
    LoginComponent,
    LocationComponent,
    SearchComponent,
    PiczoomComponent
  ],
  imports: [
    RouterModule.forRoot([
    { path: '', component: HomeComponent },
    { path: 'collection/:id', component: CollectionComponent },
    { path: 'collection-list', component: CollectionListComponent },
    { path: 'tag/:tag', component: TagComponent },
    { path: 'location/:id/:loc', component: LocationComponent },
    { path: 'account/:username', component: AccountComponent },
    { path: 'single-post/:id', component: SinglePostComponent },
    { path: 'popular', component: PopularComponent },
    { path: 'popular/:tab', component: PopularComponent },
    { path: 'collection-list', component: CollectionListComponent },
    { path: 'about', component: AboutComponent },
    { path: 'search', component: NearbySearchComponent },
    { path: 'search/:tab/:value', component: NearbySearchComponent },
    { path: 'login', component: LoginComponent },
    { path: 'pic-zoom', component: PiczoomComponent },
], { enableTracing: true, initialNavigation: 'enabled' } // <-- debugging purposes only
),
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    NgbModule,
    FontAwesomeModule,
    InfiniteScrollModule,
    NgxUiLoaderModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatInputModule,
    NbThemeModule.forRoot(),
    DateFnsModule.forRoot(),
    NgxLinkifyjsModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
